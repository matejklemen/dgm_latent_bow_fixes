
# The Latent Bag of Words Model 

Implementation* of Yao Fu, Yansong Feng and John Cunningham, _Paraphrase Generation with Latent Bag of Words_. NeurIPS 2019. [paper](https://github.com/FranxYao/dgm_latent_bow/blob/master/doc/latent_bow_camera_ready.pdf) 

\* This is not the official repository. It contains fixes and additions for my need. The official repository is 
available at [FranxYao/dgm_latent_bow](https://github.com/FranxYao/dgm_latent_bow).

## Reproduce 

```bash 
$ mkdir models
$ mkdir outputs
$ cd src
$ python3 main.py 
```

May need to install nltk stopwords first, just follow the prompt

## Data 

The `master` branch is currently tweaked for some other custom dataset that is not public at the moment.

The `quora` branch contains the Quora dataset splits as described in most previous paraphrase generation references. 
The authors of original implementation of this model use a different split for some reason.

## Code Structure

The core implementation is in the following files: 

* config.py 
* main.py 
* controller.py 
* latent_bow.py 

## Additional changes

* added early stopping with hardcoded tolerance of 5 steps
* added loss as a possible early stopping criteria
